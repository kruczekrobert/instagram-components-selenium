from components.signin_component import SigninComponent
from components.profile_component import ProfileComponent
import time

signin_comp = SigninComponent()
signin_comp.open_signin_page()
signin_comp.save_form_if_exists()
signin_comp.name_input().send_keys('frailplay')
signin_comp.password_input().send_keys('stokrotka123')
signin_comp.login_button().click()

time.sleep(1)

if signin_comp.error_message_exists():
    print(signin_comp.error_message().text)
else:
    print("Welcome to the home page after logging in")

# Invalid target name
profile_comp = ProfileComponent(target="robercikxxxxx1")

if profile_comp.target_exists():
    profile_comp.open_profile_page()
