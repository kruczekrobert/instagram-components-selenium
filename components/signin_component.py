from selenium.common.exceptions import NoSuchElementException
from utils.driversGlobal import _driver, page_has_not_loaded

class SigninComponent:
    """ Signin page component reader """

    def __init__(self):
        self.form_component = None

    def open_signin_page(self):
        """ Open on driver signin page """
        _driver.get('https://instagram.com/accounts/login')
        if page_has_not_loaded():
            _driver.set_page_load_timeout(3)

    def form_exists(self):
        """ Check form component exists """
        try:
            _driver.find_element_by_css_selector('form')
        except NoSuchElementException:
            return False
        return True

    def save_form_if_exists(self):
        """ Save form component into object properties if exists """
        if self.form_exists():
            self.form_component = _driver.find_elements_by_css_selector('form')[0]
        else:
            print('[SigninComponent] FORM NOT EXISTS')

    def signin_form(self):
        """ Return signin form component """
        return self.form_component

    def name_input(self):
        """  Return name input component allowed to send keys """
        name_input_component = self.form_component.find_elements_by_css_selector('input')[0]
        return name_input_component

    def password_input(self):
        """  Return password input component allowed to send keys """
        password_input_component = self.form_component.find_elements_by_css_selector('input')[1]
        return password_input_component

    def login_button(self):
        """  Return clickable login button component """
        login_button_component = self.form_component.find_elements_by_css_selector('button')[1]
        return login_button_component

    def error_message_exists(self):
        """ Check error message component exists """
        try:
            self.form_component.find_element_by_id('slfErrorAlert')
        except NoSuchElementException:
            return False
        return True

    def error_message(self):
        """ Return error message component if exists (Wait a second after click login button to use this method) """
        if self.error_message_exists():
            return self.form_component.find_element_by_id('slfErrorAlert')
