from utils.driversGlobal import _driver, _shadow_driver
from selenium.common.exceptions import NoSuchElementException


class ProfileComponent:
    """ Profile page component reader """
    def __init__(self, target, accounts_url='https://www.instagram.com/{}/'):
        self.target = target
        self.accounts_url = accounts_url

    def target_exists(self):
        """ Check target profile page exists """
        _shadow_driver.get(self.accounts_url.format(self.target))
        try:
            _shadow_driver.find_element_by_class_name('error-container')
        except NoSuchElementException:
            return True
        print("[ProfileComponent] INVALID TARGET NAME")
        _shadow_driver.close()
        return False

    def open_profile_page(self):
        """ Open target profile page """
        _driver.get(self.accounts_url.format(self.target))
