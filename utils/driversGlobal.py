from selenium import webdriver

# Default webdriver is Chrome
_driver = webdriver.Chrome()

# Shadow webdriver
shadow_options = webdriver.ChromeOptions()
shadow_options.add_argument('--headless')
_shadow_driver = webdriver.Chrome(chrome_options=shadow_options)

def page_has_not_loaded():
    """ Return True if page has not loaded yet """
    page_state = _driver.execute_script('return document.readyState;')
    return page_state != 'complete'

